var db;

var app = angular.module('starter', ['ionic', 'loginModule'])

.config(function($httpProvider) {
  $httpProvider.interceptors.push(function($rootScope) {
    return {
      request: function(config) {
        $rootScope.$broadcast('loading:show')
        return config
      },
      response: function(response) {
        $rootScope.$broadcast('loading:hide')
        return response
      },
      finally: function() {
        $rootScope.$broadcast('loading:hide')
        return false
      }
    }
  })
})
.run(function($ionicPlatform, $cordovaSQLite,$rootScope, $ionicLoading) {
   $rootScope.$on('loading:show', function() {
        $ionicLoading.show({template: '<ion-spinner icon="ios-small" class="sem-fundo"></ion-spinner>',
                           noBackdrop: false})
   })

    $rootScope.$on('loading:hide', function() {
        $ionicLoading.hide()
    })

    $ionicPlatform.ready(function() {

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    } 
      
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
  })
  .state('app.mapa', {
      url: '/mapa',
      views: {
        'menuContent': {
          templateUrl: 'templates/mapa.html',
          controller: 'mapaController'
        }
      }
    })
  .state('app.calendario', {
      url: '/calendario',
      views: {
        'menuContent': {
          templateUrl: 'templates/calendario.html'
        }
      }
    })
  .state('app.noticias', {
      url: '/noticias',
      views: {
        'menuContent': {
          templateUrl: 'templates/noticias.html'
        }
      }
    })
  .state('app.login', {
    url: '/login',
    views: {
      'menuContent': {
        templateUrl: 'templates/login.html',
        controller: 'loginController'   
      }
    }
  })
  .state('app.loading', {
    url: '/carregando',
    views: {
      'menuContent': {
        templateUrl: 'templates/loading.html'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/login');
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('logado', {
    url: '/logado',
    abstract: true,
    templateUrl: 'templates/menu-logado.html',
  })
  .state('logado.disciplinas', {
      url: '/disciplinas',
      views: {
        'menuLogado': {
          templateUrl: 'templates/disciplinas.html',
          controller: 'disciplinasController'    
        }
      }
    })
   .state('logado.disciplina', {
      url: '/disciplina',
      views: {
        'menuLogado': {
          templateUrl: 'templates/disciplina.html',
          controller: 'disciplinasController'
        }
      }
    })
  .state('logado.mapa', {
      url: '/mapa',
      views: {
        'menuLogado': {
          templateUrl: 'templates/mapa.html',
          controller: 'mapaController'
        }
      }
    })
  .state('logado.noticias', {
      url: '/noticias',
      views: {
        'menuLogado': {
          templateUrl: 'templates/noticias.html'
        }
      }
    })
  .state('logado.torpedo', {
    url: '/torpedo',
    views: {
      'menuLogado': {
        templateUrl: 'templates/torpedo.html'
      }
    }
  })
   .state('logado.loading', {
    url: '/carregando',
    views: {
      'menuContent': {
        templateUrl: 'templates/loading.html'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/login');
});
