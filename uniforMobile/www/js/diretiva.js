app.directive("dirCarregando", ['$rootScope', function ($rootScope) {
   return {
      templateUrl: "<div ng-show='showMe'><span>Carregando</span></div>",
      link: function (scope, element, attrs) {

         $rootScope.on('loading-started', function () {
                  scope.showMe = true;
         });

         $rootScope.on('loading-complete', function () {
            scope.showMe = false;
         });
      }
   };
}]);
