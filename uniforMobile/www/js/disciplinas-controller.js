(function(){
        'use strict';

    app.controller('disciplinasController', disciplinasController);
    
    function disciplinasController($scope, $http, $cookies,$ionicLoading){

        var vmDisciplinas = this;

        $scope.disciplinas = [];
        var usuario = JSON.parse($cookies.get('usuario'));
        var request = "http://services-dev.unifor.br/services-dev/disciplinas/cursando?token="+usuario.token;

        $scope.doRefresh = function (){
            $http.get(request)
                        .success(function(retorno){
                            $scope.disciplinas = retorno.data;
                            preencherView();
                            consertarDisciplinas();
                        }).error(function(erro){
                            console.log(erro);
                        }).finally(function(){
                            $scope.$broadcast('scroll.refreshComplete');
                        });
        }

        $scope.doRefresh();

        function preencherView(){
                if ($scope.disciplinas){

                    vmDisciplinas.contador = 0;

                    console.log($scope.disciplinas);
                    vmDisciplinas.contador = $scope.disciplinas.length-1;

                }
        }

        function consertarDisciplinas(){
            var cont = 0;
            var disciplinaAtual = $scope.disciplinas[vmDisciplinas.contador];

            for(var i=0;i<$scope.disciplinas.length;i++){

                if (disciplinaAtual.nome == $scope.disciplinas[i].nome){
                    cont++;
                    if(cont >= 2 ){
                        $scope.disciplinas[i].nome = "";
                    }
                }

            }

            cont = 0;

            if(vmDisciplinas.contador > 0){
                vmDisciplinas.contador--;
                consertarDisciplinas();
            }
        }

    }
    
})();


