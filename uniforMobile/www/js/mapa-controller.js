(function(){

    'use strict';

    app.controller('mapaController', function($scope, $ionicLoading, $compile,$http,$ionicModal, $filter,$interval) {



    $scope.locais = [];
    $scope.locaisFiltrados = [];
    $scope.minhasDistancias = [];
    $scope.distance = "";
    $scope.myLocation = {};
    $scope.isNavigation = false;
    var mapaObject = this;
    mapaObject.path = "";
    mapaObject.infoWindows = [];

    function putMyLocationMarker(){
        var marker = new google.maps.Marker({
        position: $scope.map.getCenter(),
            icon: {
                    path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                    scale: 10
            },
            draggable: true,
            map: $scope.map
        });
    }

    $scope.localAtual =  function(local){
        if(navigator.geolocation) {
              var browserSupportFlag = true;
              navigator.geolocation.getCurrentPosition(function(position) {
                  var initialLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
                  $scope.myLocation = {lat: position.coords.latitude, lng: position.coords.longitude};
                  $scope.map.setCenter(initialLocation);
                  putMyLocationMarker();
                  hideLoading();
              }, function() {
                  hideLoading();
                  handleNoGeolocation(browserSupportFlag);
              });
        }
        // Browser doesn't support Geolocation
        else {
            browserSupportFlag = false;
            hideLoading();
            handleNoGeolocation(browserSupportFlag);
        }

    }

      function handleNoGeolocation(errorFlag) {
            if (errorFlag == true) {
                alert("Geolocation service failed.");
                initialLocation = newyork;
            } else {
                alert("Your browser doesn't support geolocation. We've placed you in Siberia.");
                initialLocation = siberia;
            }
        $scope.map.panTo(initialLocation);
      }

      function initialize() {
        var myLatlng = new google.maps.LatLng(-3.769156,-38.479566);

        var mapOptions = {
          center: myLatlng,
          zoom: 18,
          disableDefaultUI: true,
          mapMaker: true,
          mapTypeId: google.maps.MapTypeId.HYBRID
        };
        var map = new google.maps.Map(document.getElementById("map"),
            mapOptions);



        var coordenadasUnifor = [
                                    {lat: -3.768120, lng: -38.482060},
                                    {lat: -3.768000, lng: -38.480892},
                                    {lat: -3.766476, lng: -38.480999},
                                    {lat: -3.765808, lng: -38.474527},
                                    {lat: -3.766439, lng: -38.473753},
                                    {lat: -3.768031, lng: -38.473689},
                                    {lat: -3.768339, lng: -38.473081},
                                    {lat: -3.770778, lng: -38.474546},
                                    {lat: -3.771423, lng: -38.481763}
                                ];

        var unifor = new google.maps.Polygon ({
                                            paths: coordenadasUnifor,
                                            strokeColor: '#0055FF',
                                            strokeOpacity: 0.5,
                                            strokeWeight: 6,
                                            fillOpacity: 0
                                        });
        unifor.setMap(map);

          //REQUEST DOS LOCAIS
          $http.get('http://services-dev.unifor.br/services-dev/localizacao/todos')
            .success(function(locais){

              $scope.locais = locais.data;

              $scope.ordenarLocais();

              $scope.locaisFiltrados.forEach(function(local){
                  var location = {lat: local.latitude, lng: local.longitude};

                  var contentString = "<h1 class='info-window'>"+local.titulo+"</h1>"+local.descricao;

                  var infowindow = new google.maps.InfoWindow({
                        content: contentString
                  });

                 var marker = new google.maps.Marker({
                    position: location,
                    fillColor: '#FF00FF',
                    map: map,

                 });

                  marker.addListener('click', function() {
                        infowindow.open(map, marker);
                  });

                  mapaObject.infoWindows.push(infowindow);

                  marker.addListener('rightclick', function() {
                        infowindow.close();
                  });

              });

            }).error(function(erro){
                alert(erro);
            });

        $scope.localAtual();

        $scope.map = map;

      }//fim initialize


      google.maps.event.addDomListener(map, "click", function(event) {
                for (var i = 0; i < mapaObject.infoWindows.length; i++ ) {
                        mapaObject.infoWindows[i].close();
                }
      });

      $scope.cancelNavigation = function(){
             if(mapaObject.path){
                 mapaObject.path.setMap(null);
                 $scope.isNavigation = false;
                 moveCameraTo();
             }
        }

        $scope.tracarRota = function(local){

            $scope.closeModal();
            $scope.isNavigation = true;
            var coordinates = [
                                {lat: local.latitude, lng: local.longitude},
                                {lat: $scope.myLocation.lat, lng: $scope.myLocation.lng}
                              ];


            mapaObject.path = new google.maps.Polyline({
                        path: coordinates,
                        geodesic: true,
                        strokeColor: '#FFFF66',
                        strokeOpacity: 0.7,
                        scale: 3,
                        strokeWeight: 5
                });

            mapaObject.path.setMap($scope.map);

            moveCameraTo(local);

        }

      google.maps.event.addDomListener(window, 'load', initialize);

      initialize();

      $scope.ordenarLocais = function(){
           var tipoLocal = "";
           $scope.locaisFiltrados = [];
           $scope.locais.forEach(function(local, index){
               if(index == 0) {
                   tipoLocal = local.tipoLocal;
               }else{
                   if(local.tipoLocal == 'PERIMETRO' || local.titulo == ""){
                       return;
                   }
                   if(!tipoLocal.localeCompare(local.tipoLocal)){
                       local.tipoLocal = '';
                   }else{
                       tipoLocal = local.tipoLocal;
                   }
               }
               $scope.locaisFiltrados.push(local);
            });

          //console.log($scope.locaisFiltrados);

       }

        function moveCameraTo(local){
            if(local)
                $scope.map.panTo(new google.maps.LatLng(local.latitude, local.longitude));
            else
                $scope.map.panTo(new google.maps.LatLng($scope.myLocation.lat, $scope.myLocation.lng));
        }

          //GEOLOCATION
          $scope.centerOnMe = function() {
            if(!$scope.map) {
              return;
            }

              startaLoading();

              $scope.localAtual();

          };

        function startaLoading(){
            $scope.loading = $ionicLoading.show({
              content: 'Getting current location...',
              showBackdrop: false
            });
            return this;
        }

        function hideLoading(){
            $ionicLoading.hide();
        }

            var rad = function(x) {
                    return x * Math.PI / 180;
            };

            var getDistance = function(p1, p2) {
                  var R = 6378137; // Earth’s mean radius in meter
                  var dLat = rad(p2.latitude - p1.lat);
                  var dLong = rad(p2.longitude - p1.lng);
                  var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                    Math.cos(rad(p1.lat)) * Math.cos(rad(p2.latitude)) *
                    Math.sin(dLong / 2) * Math.sin(dLong / 2);
                  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                  var d = R * c;
                  return d; // returns the distance in meter
            };

        //MODAL
        $ionicModal.fromTemplateUrl('my-modal.html', {
                scope: $scope,
                animation: 'slide-in-up'
         }).then(function(modal) {
        $scope.modal = modal;
         });
          $scope.openModal = function() {
            startaLoading();
            $.get($scope.localAtual()).done(function(){
                   $scope.locaisFiltrados.forEach(function(local){
                        var distancia = getDistance($scope.myLocation, local);
                        $scope.minhasDistancias.push(distancia.toFixed(0));
                   });
                   hideLoading();
                   $scope.modal.show();
            });


          };
          $scope.closeModal = function() {
            $scope.modal.hide();
          };
          //Cleanup the modal when we're done with it!
          $scope.$on('$destroy', function() {
            $scope.modal.remove();
          });
          // Execute action on hide modal
          $scope.$on('modal.hidden', function() {
            // Execute action
          });
          // Execute action on remove modal
          $scope.$on('modal.removed', function() {
            // Execute action
          });//FIM DO MODAL

    });

})();

